import { createStore, applyMiddleware } from 'redux';
import Immutable from 'immutable';
import rootReducer from '../reducers';
import io from 'socket.io-client';
import createSocketIoMiddleware from 'redux-socket.io';
let socket = io('http://test.bearlab.io:3000');
let socketIoMiddleware = createSocketIoMiddleware(socket, "server/");

const initialState = Immutable.Map();

export default createStore(
    rootReducer,
    initialState,
    applyMiddleware(socketIoMiddleware)
);
