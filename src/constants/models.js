import Immutable from 'immutable';

export const ChatState = Immutable.fromJS({
    messages:[],
    message: ''
});
