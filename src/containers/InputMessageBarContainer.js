import { connect } from 'react-redux';
import { sendMessage, setInMessage } from '../actions';
import InputMessageBar from '../components/InputMessageBar';

export default connect(
    (state) => ({
        message: state.get('message'),
    }),
    (dispach) => ({
        onEnter: (text) => {
            dispach(sendMessage());
            dispach({type:'server/sendMessage', data: text});
        },
        onChangeText: (text) => (
            dispach(setInMessage(text))
        )
    })

)(InputMessageBar);
