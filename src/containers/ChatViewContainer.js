import { connect } from 'react-redux';
import ChatView from '../components/ChatView';
import { receiveMessage } from '../actions';

export default connect(
    (state) => ({
        messages: state.get('messages'),
    }),
    (dispach) => ({})
)(ChatView);
