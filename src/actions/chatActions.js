import { createAction } from 'redux-actions';

import {
    SEND_MESSAGE,
    RECEIVE_MESSAGE,
    SET_IN_MESSAGE,
} from '../constants/actionTypes';

export const sendMessage = createAction(SEND_MESSAGE);
export const receiveMessage = createAction(RECEIVE_MESSAGE);
export const setInMessage = createAction(SET_IN_MESSAGE);
