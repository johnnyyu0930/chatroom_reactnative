import { handleActions } from 'redux-actions';
import { ChatState } from '../constants/models';
import { SEND_MESSAGE, RECEIVE_MESSAGE, SET_IN_MESSAGE } from '../constants/actionTypes';

const chatReducers = handleActions({
    SEND_MESSAGE: (state, { payload }) => (
        state
    ),
    RECEIVE_MESSAGE: (state, message) => (
        state.set(
            'messages',
            state.get('messages').push(message)
        )
    ),
    SET_IN_MESSAGE: (state, text) => (
        state.setIn(
            'message',
            text
        )
    ),

},ChatState)
export default chatReducers;
