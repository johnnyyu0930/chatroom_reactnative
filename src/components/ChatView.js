import React from 'react';
import ReactNative, { ScrollView ,Text } from 'react-native';


class ChatView extends React.Component {
    render() {
        let messages = this.props.messages || [];
        let texts = messages.map(({ip, message}) => (
            <Text>{ip}:{text}</Text>
        ));
        return(
            <ScrollView style={ {backgroundColor: 'pink', flex:1} }>
                {texts}
            </ScrollView>
        );
    }
}

export default ChatView;
