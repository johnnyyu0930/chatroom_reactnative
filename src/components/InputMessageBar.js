import React from 'react';
import ReactNative, { View, TextInput, Button } from 'react-native';

const InputMessageBar = (props) => (
    <View style={{ustifyContent :'flex-end', flexDirection: 'row' }}>
        <TextInput onChangeText={props.onChangeText}  style={{backgroundColor: '#b3e36d', flex: 1}} />
        <Button title='send' onPress={props.onEnter(props.message)} style={{backgroundColor: '#8bdee9'}}/>
    </View>
)

export default InputMessageBar;
