import React from 'react';
import { Provider } from 'react-redux';
import ReactNative, { View, TextInput, Button } from 'react-native';
import ChatViewContainer from '../../containers/ChatViewContainer';
import InputMessageBarContainer from '../../containers/InputMessageBarContainer';
import store from '../../store';
import createSocketIoMiddleware from 'redux-socket.io';
import io from 'socket.io-client';


class Main extends React.Component {
    constructor(props){
        super(props);
        let socket = io('http://test.bearlab.io:3000');
    }
    render(){
        return(
            <Provider store={store}>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <ChatViewContainer />
                    <InputMessageBarContainer />
                </View>
            </Provider>
        );
    }
};


export default Main;
